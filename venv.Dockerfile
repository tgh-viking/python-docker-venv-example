FROM python:3.12-bookworm as BUILD

RUN python -m venv /opt/venv

# activate venv
ENV PATH="/opt/venv/bin:$PATH"
ENV VIRTUAL_ENV="/opt/venv/bin"

WORKDIR /app
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

FROM python:3.12-slim-bookworm

# activate venv
ENV PATH="/opt/venv/bin:$PATH"
ENV VIRTUAL_ENV="/opt/venv/bin"

COPY --from=BUILD /opt/venv /opt/venv

WORKDIR /app
COPY test.py .