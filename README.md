# Explanation

Some python packages require compiling code as part of their installation, which in turn requires build tools.
The `slim`-tagged python base images do not include any build tools, so they cannot install these packages.
Changing the base image to a non-`slim` version (that is, one _with_ build tools) solves this issue, but at a cost of final image size.

As an alternative solution, we can use python virtual environments to separate the build and run stages, letting us transfer and use the built packages from a `slim` base.
In this example, it reduces the size of the final image by some 850MB.
This size reduction is overwhelmingly caused by the difference in size of the `slim` and non-`slim` base images, the actual package itself is a few megabytes at most.

The choice of using the `jsonnet` package in this example was arbitrary; it was simply the first suitable package that I found.

# Dockerfiles

- `base.Dockerfile` contains the "simple" solution, where we use a base image that contains the build tools.
- `venv.Dockerfile` contains the more complicated solution, where we build a `venv` and transfer it from one stage to the next.

# Testing for yourself

```bash
# Building images (may take a few minutes)
docker build -t python-test-base -f base.Dockerfile .
docker build -t python-test-venv -f venv.Dockerfile .

# Verifying that the images work (no errors = all good)
docker run -it --rm python-test-base python test.py
docker run -it --rm python-test-venv python test.py

# Checking final image size
docker image ls "python-test-*"
```

Sample output of last command:

```
REPOSITORY         TAG       IMAGE ID       CREATED         SIZE
python-test-venv   latest    a7a63eddcd80   3 minutes ago   163MB
python-test-base   latest    63de47878bfd   4 minutes ago   1.05GB
```
